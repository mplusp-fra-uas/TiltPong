package dev.mplusp.tiltpong;

import android.graphics.Color;
import android.util.DisplayMetrics;

// TODO make everything display resolution independant

class Values {
    static final int MAX_FPS = 60;
    static final int MILLISECONDS_PER_SECOND = 1000;
    static final int NANOSECONDS_PER_MILLISECOND = 1000000;

    static float screenDensity;
    static int screenWidthPx;
    static int screenHeightPx;

    static float fpsCounterTextSize;
    static int fpsCounterX;
    static int fpsCounterY;

    static int ballStartX;
    static int ballStartY;
    static int ballDiameter;
    static int ballColor = Color.RED;
    static double ballStartXSpeed;
    static double ballStartYSpeed;

    static int player1Width;
    static int player1Height;
    static int player1StartX;
    static int player1StartY;
    static int player1Color = Color.BLACK;
    static double player1StartXSpeed;
    static double player1StartYSpeed;

    static int player2Width;
    static int player2Height;
    static int player2StartX;
    static int player2StartY;
    static int player2Color = Color.BLACK;
    static double player2StartXSpeed;
    static double player2StartYSpeed;

    static void initWithDisplayMetrics(DisplayMetrics displayMetrics) {
        screenDensity = displayMetrics.density;
        screenWidthPx = displayMetrics.widthPixels;
        screenHeightPx = displayMetrics.heightPixels;

        fpsCounterTextSize = 32f * screenDensity;
        fpsCounterX = (int) fpsCounterTextSize / 3;
        fpsCounterY = (int) fpsCounterTextSize;
        ballStartX = screenWidthPx / 2;
        ballStartY = screenHeightPx / 2;
        ballDiameter = screenWidthPx / 12;

        ballStartXSpeed = (double) screenWidthPx / 1200;
        ballStartYSpeed = (double) screenHeightPx / 1500;

        player1Width = player2Width = screenWidthPx / 14;
        player1Height = player2Height = screenHeightPx / 3;
        player1StartX = player1Width;
        player2StartX = screenWidthPx - 2 * player1Width;
        player1StartY = player2StartY = screenHeightPx / 3;
        player1StartXSpeed = player2StartXSpeed = 0;
        player1StartYSpeed = (double) screenHeightPx / 1000;
        player2StartYSpeed = -player1StartYSpeed;
    }

}
