package dev.mplusp.tiltpong;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import static dev.mplusp.tiltpong.Values.screenHeightPx;
import static dev.mplusp.tiltpong.Values.screenWidthPx;

public class MovableGameObject implements GameObject {
    private long lastUpdateTimeMs;
    Rect rect;
    int color;
    private double xSpeed;
    private double ySpeed;

    MovableGameObject(Rect rect, int color, double xSpeed, double ySpeed) {
        this.rect = rect;
        this.color = color;
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
        lastUpdateTimeMs = System.currentTimeMillis();
    }

    @Override
    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(color);
        canvas.drawRect(rect, paint);
    }

    @Override
    public void update() {
        long elapsedTimeMs = System.currentTimeMillis() - lastUpdateTimeMs;
        lastUpdateTimeMs = System.currentTimeMillis();
        rect.set( rect.left + (int) (elapsedTimeMs * xSpeed),
                rect.top + (int) (elapsedTimeMs * ySpeed),
                rect.right + (int) (elapsedTimeMs * xSpeed),
                rect.bottom + (int) (elapsedTimeMs * ySpeed)
        );
    }

    double getXSpeed() {
        return xSpeed;
    }

    void setXSpeed(double xSpeed) {
        this.xSpeed = xSpeed;
    }

    double getYSpeed() {
        return ySpeed;
    }

    void setYSpeed(double ySpeed) {
        this.ySpeed = ySpeed;
    }

    void moveLeftToX(int x) {
        rect.offsetTo(x, rect.top);
    }

    void moveRightToX(int x) {
        int width = rect.width();
        rect.offsetTo(x - width, rect.top);
    }

    void moveTopToY(int y) {
        rect.offsetTo(rect.left, y);
    }

    void moveBottomToY(int y) {
        int height = rect.height();
        rect.offsetTo(rect.left, y - height);
    }

    boolean collidesWithLeftScreenBorder() {
        return rect.left <= 0;
    }

    boolean collidesWithRightScreenBorder() {
        return rect.left >= screenWidthPx;
    }

    boolean collidesWithTopScreenBorder() {
        return rect.top <= 0;
    }

    boolean collidesWithBottomScreenBorder() {
        return rect.bottom >= screenHeightPx;
    }

    boolean leftCollidesWithRight(MovableGameObject movableGameObject) {
        return Rect.intersects(rect, movableGameObject.rect)
                && rect.left <= movableGameObject.rect.right;
    }

    boolean rightCollidesWithLeft(MovableGameObject movableGameObject) {
        return Rect.intersects(rect, movableGameObject.rect)
                && rect.right >= movableGameObject.rect.left;
    }

}
