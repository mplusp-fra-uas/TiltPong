package dev.mplusp.tiltpong;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

import static dev.mplusp.tiltpong.Values.MAX_FPS;
import static dev.mplusp.tiltpong.Values.MILLISECONDS_PER_SECOND;
import static dev.mplusp.tiltpong.Values.NANOSECONDS_PER_MILLISECOND;

public class GameLoopThread extends Thread {

    private SurfaceHolder surfaceHolder;
    private GameSurfaceView gameSurfaceView;
    private boolean running;
    long averageFps;

    void setRunning(boolean running) {
        this.running = running;
    }

    GameLoopThread(SurfaceHolder surfaceHolder, GameSurfaceView gameSurfaceView) {
        super();
        this.surfaceHolder = surfaceHolder;
        this.gameSurfaceView = gameSurfaceView;
    }

    @Override
    public void run() {
        long startTimeNs;
        long timeMs;
        long waitTimeMs;
        int frameCount = 0;
        long totalTimeNs = 0;
        long targetTime = MILLISECONDS_PER_SECOND / MAX_FPS;

        while (running) {
            startTimeNs = System.nanoTime();
            Canvas canvas = null;

            try {
                canvas = surfaceHolder.lockCanvas();
                gameSurfaceView.update();
                gameSurfaceView.draw(canvas);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (canvas != null) {
                    try {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            timeMs = (System.nanoTime() - startTimeNs) / NANOSECONDS_PER_MILLISECOND;
            waitTimeMs = targetTime - timeMs;

            try {
                if (waitTimeMs > 0) {
                    sleep(waitTimeMs);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            totalTimeNs += System.nanoTime() - startTimeNs;
            frameCount++;

            if (frameCount == MAX_FPS) {
                averageFps = MILLISECONDS_PER_SECOND / (totalTimeNs / frameCount / NANOSECONDS_PER_MILLISECOND);
                frameCount = 0;
                totalTimeNs = 0;
            }
        }
    }

}
