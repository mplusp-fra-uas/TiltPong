package dev.mplusp.tiltpong;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import static dev.mplusp.tiltpong.Values.ballColor;
import static dev.mplusp.tiltpong.Values.ballDiameter;
import static dev.mplusp.tiltpong.Values.ballStartX;
import static dev.mplusp.tiltpong.Values.ballStartXSpeed;
import static dev.mplusp.tiltpong.Values.ballStartY;
import static dev.mplusp.tiltpong.Values.ballStartYSpeed;
import static dev.mplusp.tiltpong.Values.fpsCounterTextSize;
import static dev.mplusp.tiltpong.Values.fpsCounterX;
import static dev.mplusp.tiltpong.Values.fpsCounterY;
import static dev.mplusp.tiltpong.Values.player1Color;
import static dev.mplusp.tiltpong.Values.player1Height;
import static dev.mplusp.tiltpong.Values.player1StartX;
import static dev.mplusp.tiltpong.Values.player1StartXSpeed;
import static dev.mplusp.tiltpong.Values.player1StartY;
import static dev.mplusp.tiltpong.Values.player1StartYSpeed;
import static dev.mplusp.tiltpong.Values.player1Width;
import static dev.mplusp.tiltpong.Values.player2Color;
import static dev.mplusp.tiltpong.Values.player2Height;
import static dev.mplusp.tiltpong.Values.player2StartX;
import static dev.mplusp.tiltpong.Values.player2StartXSpeed;
import static dev.mplusp.tiltpong.Values.player2StartY;
import static dev.mplusp.tiltpong.Values.player2StartYSpeed;
import static dev.mplusp.tiltpong.Values.player2Width;
import static dev.mplusp.tiltpong.Values.screenDensity;
import static dev.mplusp.tiltpong.Values.screenHeightPx;
import static dev.mplusp.tiltpong.Values.screenWidthPx;

public class GameSurfaceView extends SurfaceView implements SurfaceHolder.Callback {
    private GameLoopThread gameLoopThread;

    private Ball ball;
    private MovableGameObject player1;
    private MovableGameObject player2;

    private int player1Score = 0;
    private int player2Score = 0;

    public GameSurfaceView(Context context) {
        super(context);

        SurfaceHolder surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);
        gameLoopThread = new GameLoopThread(surfaceHolder, this);

        // add ball
        Rect ballRect = new Rect(ballStartX, ballStartY, ballStartX + ballDiameter, ballStartY + ballDiameter);
        ball = new Ball(ballRect, ballColor, ballStartXSpeed, ballStartYSpeed);

        // add player 1
        Rect player1Rect = new Rect(player1StartX, player1StartY, player1StartX + player1Width, player1StartY + player1Height);
        player1 = new MovableGameObject(player1Rect, player1Color, player1StartXSpeed, player1StartYSpeed);

        // add player 2
        Rect player2Rect = new Rect(player2StartX, player2StartY, player2StartX + player2Width, player2StartY + player2Height);
        player2 = new MovableGameObject(player2Rect, player2Color, player2StartXSpeed, player2StartYSpeed);

        setFocusable(true);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        SurfaceHolder surfaceHolder = getHolder();
        gameLoopThread = new GameLoopThread(surfaceHolder, this);
        gameLoopThread.setRunning(true);
        gameLoopThread.start();
    }

    public void update() {
        player1.update();
        player2.update();
        ball.update();

        handleBallCollisionsWithScreenBounds();
        handlePlayerCollisionsWithScreenBounds(player1);
        handlePlayerCollisionsWithScreenBounds(player2);

        handleBallCollisionsWithLeftPlayer(player1);
        handleBallCollisionsWithRightPlayer(player2);
    }

    private void handleBallCollisionsWithScreenBounds() {
        if (ball.collidesWithLeftScreenBorder()) {
            ball.setXSpeed(-ball.getXSpeed());
            ball.moveLeftToX(ballStartX);
            player2Score++;
        }
        if (ball.collidesWithRightScreenBorder()) {
            ball.setXSpeed(-ball.getXSpeed());
            ball.moveRightToX(ballStartX);
            player1Score++;
        }
        if (ball.collidesWithTopScreenBorder()) {
            ball.setYSpeed(-ball.getYSpeed());
            ball.moveTopToY(1);
        }
        if (ball.collidesWithBottomScreenBorder()) {
            ball.setYSpeed(-ball.getYSpeed());
            ball.moveBottomToY(screenHeightPx - 1);
        }
    }

    private void handleBallCollisionsWithLeftPlayer(MovableGameObject player) {
        if (ball.leftCollidesWithRight(player)) {
            ball.setXSpeed(-ball.getXSpeed());
            ball.moveLeftToX(player.rect.right + 1);
        }
    }

    private void handleBallCollisionsWithRightPlayer(MovableGameObject player) {
        if (ball.rightCollidesWithLeft(player)) {
            ball.setXSpeed(-ball.getXSpeed());
            ball.moveRightToX(player.rect.left - 1);
        }
    }

    private void handlePlayerCollisionsWithScreenBounds(MovableGameObject player) {
        if (player.collidesWithTopScreenBorder()) {
            player.setYSpeed(-player.getYSpeed());
            player.moveTopToY(1);
        }
        if (player.collidesWithBottomScreenBorder()) {
            player.setYSpeed(-player.getYSpeed());
            player.moveBottomToY(screenHeightPx - 1);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        while (retry) {
            try {
                gameLoopThread.setRunning(false);
                gameLoopThread.join();
                retry = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        drawBackground(canvas);
        drawFps(canvas);
        drawPlayerScore(1, canvas);
        drawPlayerScore(2, canvas);
        player1.draw(canvas);
        player2.draw(canvas);
        ball.draw(canvas);
    }

    private void drawBackground(Canvas canvas) {
        canvas.drawColor(Color.WHITE);
    }

    private void drawFps(Canvas canvas) {
        Paint paint = new Paint();
        paint.setTextSize(fpsCounterTextSize);
        paint.setColor(Color.RED);
        canvas.drawText("" + gameLoopThread.averageFps, fpsCounterX, fpsCounterY, paint);
    }

    // TODO: redo this
    private void drawPlayerScore(int player, Canvas canvas) {
        Paint paint = new Paint();
        paint.setTextSize(fpsCounterTextSize);

        int playerScore;
        int x;
        int y = (int) (40 * screenDensity);

        if (player == 1) {
            playerScore = player1Score;
            paint.setColor(player1Color);
            x = screenWidthPx / 4;
        } else {
            playerScore = player2Score;
            paint.setColor(player2Color);
            x = 3 * (screenWidthPx / 4);
        }

        canvas.drawText("" + playerScore , x, y, paint);
    }
}
