package dev.mplusp.tiltpong;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class Ball extends MovableGameObject {

    Ball(Rect rect, int color, double xSpeed, double ySpeed) {
        super(rect, color, xSpeed, ySpeed);
    }

    @Override
    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(color);
        canvas.drawCircle(rect.left + rect.width() / 2,
                rect.top + rect.width() / 2,
                rect.width() / 2,
                paint
        );
    }

}
