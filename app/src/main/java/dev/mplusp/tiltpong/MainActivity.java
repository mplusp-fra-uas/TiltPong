package dev.mplusp.tiltpong;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import static dev.mplusp.tiltpong.Values.screenHeightPx;
import static dev.mplusp.tiltpong.Values.screenWidthPx;

public class MainActivity extends Activity {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        makeActivityFullScreen();
        updateDisplayMetrics();

        // set GameSurfaceView as this activity's content view
        setContentView(new GameSurfaceView(this));
    }

    @Override
    protected void onResume() {
        super.onResume();

        setOrientationToLandscape();
        updateDisplayMetrics();
    }

    private void makeActivityFullScreen() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    private void setOrientationToLandscape() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    private void updateDisplayMetrics() {
        WindowManager windowManager = getWindowManager();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);

        Values.initWithDisplayMetrics(displayMetrics);
        Log.d(TAG, "screenWidthPx: " + screenWidthPx + ", screenHeightPx: " + screenHeightPx);
    }

}
